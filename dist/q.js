
function Q(selector) {
    if(this instanceof Q) {
        // Constructor check -> in case you forgot to put 'new' it will do that for you.
        if(selector.indexOf('.') !== -1) {
            this['node'] = document.getElementsByClassName(selector.split('.').join(''));
        } else if(selector.indexOf('#') !== -1) {
            this['node'] = document.getElementById(selector.split('#').join(''));
        } else {
            this['node'] = document.getElementsByTagName(selector);
        }
    } else {
        return new Q(selector);
    }
};

Q.prototype.find = function(selector) {
    var selected = this.node.querySelector(selector);
   return selected ? selected : console.log('Q: ' + selector + ' is empty.');
};
Q.prototype.isntNull = function() {
        return this['node'] != null;
};
Q.prototype.event = function(ev, f) {
    f = f.bind(this);
    var cachedProp = this.node;
        if(this.isntNull() && cachedProp.length !== 0)  {
            this.each(function(e) {
                e.addEventListener(ev, f);
            });
        }
};

Q.prototype.click = function(f) {
        f = f.bind(this);
        var cachedProp = this.node;
        if(this.isntNull() && cachedProp.length !== 0)  {
            this.each(function(e) {
                e.addEventListener('click', f);
            });
        }
    };
Q.prototype.forEach = function(list, f) {
        for (var i = 0; i < list.length && !f(list[i], i++);) {}
    };
Q.prototype.type = function(node) {
    return node ? {}.toString.call(node) : {}.toString.call(this.node);
};
Q.prototype.each = function(f) {
        var type = {}.toString;
        if (this.type(this['node']) === '[object HTMLCollection]' || this.type(this['node']) === '[object NodeList]' ) {
            return this.forEach(this['node'], f);
        } else {
            return f(this['node']);
        }
    };

Q.prototype.html = function(html) {
    this.each(function(e) {
        e.innerHTML = html;
    });
};

Q.prototype.interval = function(f,time,infinite) {
    f = f.bind(this);
    var interval = (infinite  || false) ? setInterval : setTimeout;
    interval(f,time);
};


