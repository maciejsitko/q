
_ = {};

_.extend = function(dest,src) {
    for(var k in src)
        dest[k] = src[k];

    return dest;
};

var Stage = {},
    Circles = {};
Stage.one = Q('#stage-1');

function Ball(selector) {
    Q.call(this,selector);
}

Ball.prototype = Object.create(Q.prototype);

Ball.prototype.iterator = {
    x: 0,
    y: 0
};

Ball.prototype.randomizeColors = function() {

    function randomRGB() {
      return Math.floor(Math.random() * 256 ); 
    }
    
    var color = 'rgb(';
        color += randomRGB() + ',';
        color += randomRGB() + ',';
        color += randomRGB() + ')';

    this.node.style.background = color;
}

//--> parent container val on resize? <--//

Ball.prototype.startAnimation =function() {

    this.interval(function() {

      this.iterator.x = ++this.iterator.x % 10;
      this.iterator.y = this.iterator.x === 0 ? (++this.iterator.y % 3) : (this.iterator.y % 3);
      var coordX = this.iterator.x *10;
      var coordY = this.iterator.y *200;
      coordY = this.iterator.y * ((Stage.one.node.clientHeight * 0.25) / Stage.one.node.clientHeight);
        this.node.style.position = 'absolute';
        this.node.style.left =  coordX + '%';
        this.node.style.top =  coordY*100 + '%';
        this.adjust();
        this.randomizeColors();

    },1000, true);   
}


Ball.prototype.adjust = function() {

    var width = window.innerWidth * 0.095;
    var fontSize = window.innerWidth * 0.0015;

    this.node.style.width = width + 'px';
    this.node.style.height = width + 'px';

    Stage.one.node.style.height = width * 2 + 'px';

};

Circles.one = new Ball('#test');
Circles.two = new Ball('#test-2');

Circles.one.startAnimation();
Circles.two.startAnimation();


window.addEventListener('resize', function() {
    Circles.one.adjust();
    Circles.two.adjust();

});